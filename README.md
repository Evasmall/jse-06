**EV-TASK-MANAGER**

- **Application:** JAVA lesson application. Output terminal parameters.

- **OS:** Windows, Linux, Mac 
- **Tech Stack:** Java CE, Apache Maven, IntelliJ IDEA CE, Git 
- **Software:** Java OpenJDK 11.0.7, Apache Maven 3.6.1
- **Hardware:** CPU 64-bit, RAM 8Gb, HDD 80Gb

- **Developer:** Smolkina Evgeniya
- **Email:** smolkina_ev@nlmk.com
- **Backup:** https://github.com/Evasmall/jse

| Build |
| ------ |
| mvn clean install |

| Start |
| ------ |
| java -jar target/ev-task-manager-1.0.6.jar |

| Terminal commands | Example |
| ------ | ------ |
| version - Display program version. | java -jar ev-task-manager-1.0.6.jar version |
| about - Display developer info. | java -jar ev-task-manager-1.0.6.jar about |
| help - Display list of terminal commands. | java -jar ev-task-manager-1.0.6.jar help |
| exit - Terminate console application. | java -jar ev-task-manager-1.0.6.jar exit |